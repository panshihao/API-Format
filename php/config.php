<?php
/*
 * 设置字符编码
*/
header("content-type:text/html; charset=utf-8");
/*
 * 将默认时区设置为中国上海
*/
date_default_timezone_set('Asia/Shanghai');
$apiTxtDir = '../apis';

/**
 * 获取版本列表
 * @param unknown $apiTxtDir
 * @return multitype:
 */
function getVersionList($apiTxtDir){
	// 获取apis目录列表
	$apisDir = scandir($apiTxtDir);
	$versionArray = array();
	
	for ($i = 0 ; $i < count($apisDir) ; $i ++){
	
		// 表示这个文件是一个.api文件
		if(substr_count($apisDir[$i], '.api')){
				
			// 文件的完整名称，不包含路径
			$filename = $apisDir[$i];
			// 排除.api 之后的文件名称
			$fileContent = substr($filename, 0, strpos($filename, '.api'));
	
			// 基于_分割内容
			$fileContentSplit = explode('_',$fileContent);
			// 目前格式是固定的
			if(count($fileContentSplit) != 2){
				// 格式不正常返回跳过这个文件
				continue;
			}else{
				$api = array();
				$api['version']= $fileContentSplit[0];
				$api['tempstamp'] = $fileContentSplit[1];
				$api['filename'] = $filename;
				
				// 将api 加入到array中
				array_push($versionArray, $api);
			}
				
		}
	
	}
	
	usort($versionArray, 'versionSort');
	
	return $versionArray;
}
function versionSort($a, $b){

	if($a['version'] > $b['version']){
		return 1;
	}else if($a['version'] < $b['version']){
		return -1;
	}else{
		return 0;
	}

}