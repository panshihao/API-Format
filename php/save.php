<?php
include 'config.php';

if(isset($_POST['content'])){
	
	$versionList = getVersionList($apiTxtDir);
	
	$lastVersion = $versionList[count($versionList) - 1];
	
	if(!isset($lastVersion)){
		$lastVersion = array();
		$lastVersion['version'] = 0;
	}
	
	$timestamp = date('YmdHis');
	$newVersion = $lastVersion['version'] + 1;
	$newFilename = $newVersion . '_' . $timestamp . '.api';
	
	$newAPI = array();
	$newAPI['version'] = $newVersion;
	$newAPI['timestamp'] = $timestamp;
	$newAPI['filename'] = $newFilename;
	
	$resultCount = file_put_contents($apiTxtDir . '/'. $newFilename, $_POST['content']);
	
	$newAPI['size'] = $newAPI;
	
	echo json_encode($newAPI);
	
}else{
	echo -1;
}
