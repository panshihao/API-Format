<?php
include 'config.php';

/**
 * getversion.php的作用是获取api的版本列表
 * 通常都是先调用这个PHP再获取实际内容
 * 所有API文件都是用.api作为后缀名
 * 标准格式：版本号_20140521133355.api
 */

echo json_encode(getVersionList($apiTxtDir));
