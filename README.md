#API Format

+ 这是一个快速编写漂亮的RESTful风格API的工具。
+ 提供一种易读的易维护的构造方式
+ 提供在线编辑工具，任何时间任何地点，快速编译API内容。

## DEMO
DEMO 地址 http://panshihao.cn/apiformat/

## 安装
+ 1 将所有的源码git到本地，这是一个JS+HTML+PHP的项目你可以使用任意工具来编辑。
+ 2 直接运行 index.html 就可以看到效果了
+ 3 如果打开 index.html后无法看到效果，多半的情况是使用了不允许跨域的浏览器，那么你只要将相关的文件扔到任意的WEB环境下再访问 index.html就可以了。
+ 4 自动保存需要使用到PHP，如需使用请将项目拷贝至PHP的WEB目录下运行

## 使用
+ 1 apiformat所有的内容都来源于apis中的.api文件，通过api.js对于.api文件的解析，将内容构造到index.html中
+ 2 .api文件使用了一种类似于 markdown和api blueprint的格式，很好理解，具体的操作手册稍候会增加上来。其实.api中已经有不少的demo数据了，看一下就明白了。

## 更新
请使用邮件与我联系 312451824@qq.com
工具的更新时间不固定~~~